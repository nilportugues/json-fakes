# README #

## NOTIFICATION API



## FAVORITES API

## SEARCH HISTORY API




## CHAT WEBSOCKET SERVERS

- Endpoint to know the IP and port to connect to.
- Internally Erlang will connect all the nodes and notify the users connected together.
- Websocket servers will write to chat api to keep track.
- Shares same URL as Chat API.

## CHAT API


### Return user conversation data

Contains a list of "conversation data" (see below) for the current user.

- `/chat/users/<user_id>/conversations`
- `/chat/users/f66edb9b-857c-4131-b2f7-57de9d368263/conversations`

```js
{
	"data": [],
	"meta" : {}
}
```


### Return user blocked conversation data

Contains a list of "blocked conversation data" (see below) for the current user.

- `/chat/users/<user_id>/blocked`
- `/chat/users/f66edb9b-857c-4131-b2f7-57de9d368263/blocked`

```js
{
	"data": [],
	"meta" : {}
}
```


### Return conversations

Array containing data such as user creator and creation date and last message.

- `/chat/conversations`
- `/chat/conversations`

```js
{
	"data": [],
	"meta" : {}
}
```

### Return conversation data

Contains data such as user creator and creation date and last message.

- `/chat/conversations/<conversation_id>`
- `/chat/conversations/27cf5ba1-f5be-46a1-a9c1-1a21446945f7`

```js
{
	
}
```

### Return blocked

Array containing blocked data such as user blocked and creation date.

- `/chat/blocked`
- `/chat/blocked`

```js
{
	"data": [],
	"meta" : {}
}
```

### Return blocked data

Contains blocked data such as user blocked and creation date.

- `/chat/blocked/<blocked_id>`
- `/chat/blocked/f66edb9b-857c-4131-b2f7-57de9d368263`

```js
{
	
}
```

### Return conversation messages

Contains a paginated list of messages. Newest message first.

- `/chat/conversations/<conversation_id>/messages`
- `/chat/conversations/27cf5ba1-f5be-46a1-a9c1-1a21446945f7/messages`

```js
{
	"data": [],
	"meta" : {}
}
```


### Return total of unread messages for all conversations

Returns the total number of unread messages for a user (badge notification anyone?) and the list of message ids unread.

- `/chat/conversations/unread/<user_id>`
- `/chat/conversations/unread/f66edb9b-857c-4131-b2f7-57de9d368263`

```js
{
	"data": [],
	"meta" : {}
}
```


### Return total of unread messages per conversation

Returns the total number of unread messages per conversation and the list of message ids unread.

- `/chat/conversations/<conversation_id>/unread/<user_id>`
- `/chat/conversations/27cf5ba1-f5be-46a1-a9c1-1a21446945f7/unread/f66edb9b-857c-4131-b2f7-57de9d368263`

```js
{
	"data": [],
	"meta" : {}
}
```